/*
 * Proyecto integrador Grupo 3 2017
 * 
 * Comedor Universitario
 * Allochis, José
 * Silvera, Nicolás
 * Villegas, Leandro Antú
 */
package logica;

import java.util.Calendar;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author antuv
 */
public class VentasPieChart extends ChartFactory {

    private ChartPanel pieChart;
    private boolean cargando = false;
    private int mesPeriodo;
    private int yearPeriodo;

    public VentasPieChart() {
        this.mesPeriodo = Calendar.getInstance().get(Calendar.MONTH) + 1;
        this.yearPeriodo = Calendar.getInstance().get(Calendar.YEAR);
        JFreeChart chart = createChart(this.mesPeriodo, this.yearPeriodo);
        pieChart = new ChartPanel(chart);
        pieChart.revalidate();
        pieChart.repaint();
    }

    public ChartPanel getChart() {
        return this.pieChart;
    }

    public void cambiarPeriodo(String periodo) {
                switch (periodo) {
                    case "atras":
                        yearPeriodo = (mesPeriodo == 1 ? yearPeriodo - 1 : yearPeriodo);
                        mesPeriodo = (mesPeriodo == 1 ? 12 : mesPeriodo - 1);
                        break;
                    case "adelante":
                        yearPeriodo = (mesPeriodo == 12 ? yearPeriodo + 1 : yearPeriodo);
                        mesPeriodo = (mesPeriodo == 12 ? 1 : mesPeriodo + 1);
                        break;
                }
                pieChart = new ChartPanel(createChart(mesPeriodo, yearPeriodo));
                pieChart.revalidate();
                pieChart.repaint();
                this.cargando = false;
    }

    private JFreeChart createChart(int mes, int year) {
        DefaultPieDataset dataset = new DefaultPieDataset();
        Estadistica ventas = Estadistica.getVentas(mes, year);
        int compras = ventas.getDato(0);
        int clientes = ventas.getDato(1);
        String titulo = ventas.getTitulo();
        if (compras != 0) {
            dataset.setValue("Clientes que compraron", compras);
        }
        if (clientes - compras != 0) {
            dataset.setValue("Clientes que NO compraron", clientes - compras);
        }
        JFreeChart chart = createPieChart(
                titulo, // chart title 
                dataset, // data    
                true, // include legend   
                true,
                false);
        return chart;
    }
    
    public int getMes(){
        return this.mesPeriodo;
    }
    
    public int getYear(){
        return this.yearPeriodo;
    }

    public boolean cargando() {
        return this.cargando;
    }

    public void cargando(boolean cargando) {
        this.cargando = cargando;
    }
}
