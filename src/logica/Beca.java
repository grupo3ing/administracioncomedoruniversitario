/*
 * Proyecto integrador Grupo 3 2017
 * 
 * Comedor Universitario
 * Allochis, José
 * Silvera, Nicolás
 * Villegas, Leandro Antú
 */
package logica;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import persistencia.Conexion;

public class Beca {

    private int periodo;
    private String dni;
    private String tipo;

    public Beca(String dni, String tipo, int periodo) {
        this.dni = dni;
        this.periodo = periodo;
        this.tipo = tipo;
    }

    public int getPeriodo() {
        return this.periodo;
    }

    public String getTipo() {
        return this.tipo;
    }

    public String getDni() {
        return this.dni;
    }

    public static ArrayList<Beca> getBecas() {
        ArrayList<Beca> becas = new ArrayList();
        Conexion conexion = Conexion.getInstance();
        ResultSet r = conexion.select("*", "becas", "");
        try {
            while (r.next()) {
                String dnidb = r.getString("dni");
                int periododb = r.getInt("periodo");
                String tipodb = r.getString("tipo");
                becas.add(new Beca(dnidb, tipodb, periododb));
            }
            r.close();
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        return becas;
    }

    public boolean eliminar() {
        Conexion conexion = Conexion.getInstance();
        conexion.delete("becas", "dni = '" + this.dni + "' AND periodo = '" + this.periodo + "'");
        return true;
    }

}
