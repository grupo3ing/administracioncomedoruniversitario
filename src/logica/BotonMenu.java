/*
 * Proyecto integrador Grupo 3 2017
 * 
 * Comedor Universitario
 * Allochis, José
 * Silvera, Nicolás
 * Villegas, Leandro Antú
 */
package logica;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class BotonMenu {

    private MenuDiario menu;
    private JButton button;
    private static boolean cargando = false;

    public BotonMenu() {
        this.button = new JButton();
    }

    public void setMenu(MenuDiario menu) {
        this.menu = menu;
    }

    public MenuDiario getMenu() {
        return this.menu;
    }

    public JButton getButton() {
        return this.button;
    }

    public void setFecha(int day, int month, int year) {
        this.menu = new MenuDiario(day, month, year);
    }

    public int getDay() {
        return this.menu.getDay();
    }

    public int getMonth() {
        return this.menu.getMonth();
    }

    public int getYear() {
        return this.menu.getYear();
    }

    public void setAction() {
        this.button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (!cargando) {
                    cargando = true;
                    button.setText("...");
                    Thread carga = new Thread() {
                        public void run() {

                            ArrayList<Object[]> alimentos = Alimento.getAlimentos();

                            Object[] platosprincipales = alimentos.get(0);
                            Object[] salsas = alimentos.get(1);
                            Object[] guarniciones = alimentos.get(2);
                            Object[] postres = alimentos.get(3);

                            String ppindex;
                            if (!"".equals(menu.getPlatoPrincipal())) {
                                ppindex = menu.getPlatoPrincipal();
                            } else {
                                ppindex = (String) platosprincipales[0];
                            }

                            String sindex;
                            if (!"".equals(menu.getSalsa())) {
                                sindex = menu.getSalsa();
                            } else {
                                sindex = (String) salsas[0];
                            }

                            String gindex;
                            if (!"".equals(menu.getGuarnicion())) {
                                gindex = menu.getGuarnicion();
                            } else {
                                gindex = (String) guarniciones[0];
                            }

                            String pindex;
                            if (!"".equals(menu.getPostre())) {
                                pindex = menu.getPostre();
                            } else {
                                pindex = (String) postres[0];
                            }

                            String platoprincipal = String.valueOf(JOptionPane.showInputDialog(null,
                                    "Elige un plato principal", String.valueOf(menu.getDay()) + "/" + String.valueOf(menu.getMonth()) + "/" + String.valueOf(menu.getYear()),
                                    JOptionPane.PLAIN_MESSAGE, null,
                                    platosprincipales, ppindex
                            ));

                            if ("null".equals(platoprincipal)) {
                                button.setText(String.valueOf(menu.getDay()));
                                cargando = false;
                                return;
                            } else if (platoprincipal.contains("-")) {
                                JOptionPane.showMessageDialog(null,
                                        "Plato principal no puede estar vacío.",
                                        "Error",
                                        JOptionPane.ERROR_MESSAGE);
                                button.setText(String.valueOf(menu.getDay()));
                                cargando = false;
                                return;
                            }

                            String salsa = String.valueOf(JOptionPane.showInputDialog(null,
                                    "Elige una salsa", String.valueOf(menu.getDay()) + "/" + String.valueOf(menu.getMonth()) + "/" + String.valueOf(menu.getYear()),
                                    JOptionPane.PLAIN_MESSAGE, null,
                                    salsas, sindex
                            ));

                            if ("null".equals(salsa)) {
                                button.setText(String.valueOf(menu.getDay()));
                                cargando = false;
                                return;
                            }

                            String guarnicion = String.valueOf(JOptionPane.showInputDialog(null,
                                    "Elige una guarnicion", String.valueOf(menu.getDay()) + "/" + String.valueOf(menu.getMonth()) + "/" + String.valueOf(menu.getYear()),
                                    JOptionPane.PLAIN_MESSAGE, null,
                                    guarniciones, gindex
                            ));

                            if ("null".equals(guarnicion)) {
                                button.setText(String.valueOf(menu.getDay()));
                                cargando = false;
                                return;
                            }

                            String postre = String.valueOf(JOptionPane.showInputDialog(null,
                                    "Elige un postre", String.valueOf(menu.getDay()) + "/" + String.valueOf(menu.getMonth()) + "/" + String.valueOf(menu.getYear()),
                                    JOptionPane.PLAIN_MESSAGE, null,
                                    postres, pindex
                            ));

                            if ("null".equals(postre)) {
                                button.setText(String.valueOf(menu.getDay()));
                                cargando = false;
                                return;
                            } else if (postre.contains("-")) {
                                JOptionPane.showMessageDialog(null,
                                        "Postre no puede estar vacío.",
                                        "Error",
                                        JOptionPane.ERROR_MESSAGE);
                                button.setText(String.valueOf(menu.getDay()));
                                cargando = false;
                                return;
                            }

                            menu.setPlatoPrincipal(platoprincipal);

                            if (!"-- Salsas --".equals(salsa)) {
                                menu.setSalsa(salsa);
                            } else {
                                menu.setSalsa("");
                            }
                            if (!"-- Guarniciones --".equals(guarnicion)) {
                                menu.setGuarnicion(guarnicion);
                            } else {
                                menu.setGuarnicion("");
                            }

                            menu.setPostre(postre);
                            button.setText(String.valueOf(menu.getDay()));
                            if (menu.guardar()) {
                                button.setToolTipText(menu.getTooltipText());
                                button.setBackground(new Color(253, 216, 53));
                                JOptionPane.showMessageDialog(null,
                                        "Cambios realizados correctamente.",
                                        "Éxito",
                                        JOptionPane.INFORMATION_MESSAGE);
                            } else {
                                JOptionPane.showMessageDialog(null,
                                        "Hubo un error al intentar guardar los cambios.",
                                        "Error",
                                        JOptionPane.ERROR_MESSAGE);
                            }
                            cargando = false;
                        }
                    };
                    carga.start();
                }
            }
        });
    }

}
