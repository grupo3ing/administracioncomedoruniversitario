/*
 * Proyecto integrador Grupo 3 2017
 * Comedor Universitario
 *
 * Allochis, José
 * Silvera, Nicolás
 * Villegas, Leandro Antú
 */
package presentacion;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Calendar;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import logica.VentasPieChart;
import org.jfree.ui.RefineryUtilities;

public class VentasMensualesUI extends JFrame {

    private VentasPieChart ventaspiechart;

    public VentasMensualesUI() {
        super("Ventas del mes");
        ventaspiechart = new VentasPieChart();
        setContentPane(ventaspiechart.getChart());
        setSize(560, 367);
        RefineryUtilities.centerFrameOnScreen(this);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);

        KeyListener listener = new KeyListener() {

            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent evt) {
                if (!ventaspiechart.cargando()) {
                    if (evt.getKeyCode() == KeyEvent.VK_LEFT) {
                        carga();
                        Thread cambio = new Thread() {
                            public void run() {
                                ventaspiechart.cambiarPeriodo("atras");
                                setContentPane(ventaspiechart.getChart());
                                revalidate();
                                repaint();
                            }
                        };
                        cambio.start();
                    } else if (evt.getKeyCode() == KeyEvent.VK_RIGHT && (ventaspiechart.getMes() != (Calendar.getInstance().get(Calendar.MONTH) + 1) || ventaspiechart.getYear() != Calendar.getInstance().get(Calendar.YEAR))) {
                        carga();
                        Thread cambio = new Thread() {
                            public void run() {
                                ventaspiechart.cambiarPeriodo("adelante");
                                setContentPane(ventaspiechart.getChart());
                                revalidate();
                                repaint();
                            }
                        };
                        cambio.start();
                    }
                }
            }
        };
        addKeyListener(listener);
    }

    private void carga() {
        ventaspiechart.cargando(true);
        ImageIcon cargandoIcon = new ImageIcon(getClass().getResource("/assets/cargando.gif"));
        JLabel cargandoIco = new JLabel("");
        JLabel cargandoText = new JLabel("Cargando...");
        cargandoText.setFont(new Font("Arial", 0, 48));
        cargandoIco.setIcon(cargandoIcon);
        cargandoText.setHorizontalAlignment(JLabel.CENTER);
        cargandoIco.setHorizontalAlignment(JLabel.CENTER);
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(cargandoIco, BorderLayout.CENTER);
        panel.add(cargandoText, BorderLayout.SOUTH);
        setContentPane(panel);
        getContentPane().revalidate();
        getContentPane().repaint();
        setSize(560, 367);
        revalidate();
        repaint();
    }
}
