/*
 * Proyecto integrador Grupo 3 2017
 * 
 * Comedor Universitario
 * Allochis, José
 * Silvera, Nicolás
 * Villegas, Leandro Antú
 */
package logica;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import persistencia.Conexion;

public class MenuDiario {

    private int year;
    private int month;
    private int day;
    private String platoprincipal = "";
    private String guarnicion = "";
    private String salsa = "";
    private String postre = "";

    public MenuDiario(String platoprincipal, String guarnicion, String salsa, String postre, int day, int month, int year) {
        this.platoprincipal = platoprincipal;
        this.guarnicion = guarnicion;
        this.salsa = salsa;
        this.postre = postre;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MenuDiario(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public int getDay() {
        return this.day;
    }

    public int getMonth() {
        return this.month;
    }

    public int getYear() {
        return this.year;
    }

    public void setPlatoPrincipal(String platoprincipal) {
        this.platoprincipal = platoprincipal;
    }

    public void setSalsa(String salsa) {
        this.salsa = salsa;
    }

    public void setGuarnicion(String guarnicion) {
        this.guarnicion = guarnicion;
    }

    public void setPostre(String postre) {
        this.postre = postre;
    }

    public String getPlatoPrincipal() {
        return this.platoprincipal;
    }

    public String getSalsa() {
        return this.salsa;
    }

    public String getGuarnicion() {
        return this.guarnicion;
    }

    public String getPostre() {
        return this.postre;
    }

    public String getTooltipText() {
        StringBuilder str = new StringBuilder();
        str.append("<html>Plato principal: ").append(this.platoprincipal).append("<br>");
        if (!"".equals(this.salsa)) {
            str.append("Salsa: ").append(this.salsa).append("<br>");
        }
        if (!"".equals(this.guarnicion)) {
            str.append("Guarnición: ").append(this.guarnicion).append("<br>");
        }
        str.append("Postre: ").append(this.postre).append("</html>");
        return str.toString();
    }

    public static ArrayList<MenuDiario> getMenusMes(int month, int year) {
        ArrayList<MenuDiario> menus = new ArrayList<>();
        Conexion conexion = Conexion.getInstance();
        ResultSet r = conexion.select("*", "menusdiarios", "MONTH(fecha) = " + month + " AND YEAR(fecha) = " + year);
        try {
            while (r.next()) {
                Timestamp timestamp = r.getTimestamp("fecha");
                Date fecha = timestamp;
                Calendar cal = Calendar.getInstance();
                cal.setTime(fecha);
                int ryear = cal.get(Calendar.YEAR);
                int rmonth = cal.get(Calendar.MONTH) + 1;
                int rday = cal.get(Calendar.DAY_OF_MONTH);
                String salsa;
                if (null == r.getString("salsa")) {
                    salsa = "";
                } else {
                    salsa = r.getString("salsa");
                }
                String guarnicion;
                if (null == r.getString("guarnicion")) {
                    guarnicion = "";
                } else {
                    guarnicion = r.getString("guarnicion");
                }
                menus.add(new MenuDiario(r.getString("platoprincipal"), guarnicion, salsa, r.getString("postre"), rday, rmonth, ryear));
            }
            r.close();
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        return menus;
    }

    public boolean guardar() {
        Conexion conexion = Conexion.getInstance();
        String fields;
        String values;
        String update;
        if ("".equals(this.salsa) && "".equals(this.guarnicion)) {
            fields = "platoprincipal, postre, fecha";
            values = "'" + this.platoprincipal + "', '" + this.postre + "', '" + this.year + "/" + this.month + "/" + this.day + "'";
            update = "ON DUPLICATE KEY UPDATE platoprincipal='" + this.platoprincipal + "', guarnicion=null, salsa=null, postre='" + this.postre + "'";
        } else if ("".equals(this.salsa)) {
            fields = "platoprincipal, guarnicion, postre, fecha";
            values = "'" + this.platoprincipal + "', '" + this.guarnicion + "', '" + this.postre + "', '" + this.year + "/" + this.month + "/" + this.day + "'";
            update = "ON DUPLICATE KEY UPDATE platoprincipal='" + this.platoprincipal + "', guarnicion='" + this.guarnicion + "', salsa=null, postre='" + this.postre + "'";
        } else if ("".equals(this.guarnicion)) {
            fields = "platoprincipal, salsa, postre, fecha";
            values = "'" + this.platoprincipal + "', '" + this.salsa + "', '" + this.postre + "', '" + this.year + "/" + this.month + "/" + this.day + "'";
            update = "ON DUPLICATE KEY UPDATE platoprincipal='" + this.platoprincipal + "', guarnicion=null, salsa='" + this.salsa + "', postre='" + this.postre + "'";
        } else {
            fields = "platoprincipal, salsa, guarnicion, postre, fecha";
            values = "'" + this.platoprincipal + "', '" + this.salsa + "', '" + this.guarnicion + "', '" + this.postre + "', '" + this.year + "/" + this.month + "/" + this.day + "'";
            update = "ON DUPLICATE KEY UPDATE platoprincipal='" + this.platoprincipal + "', guarnicion='" + this.guarnicion + "', salsa='" + this.salsa + "', postre='" + this.postre + "'";
        }
        return conexion.insert("menusdiarios", fields, values, update);
    }
}
