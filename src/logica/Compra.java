/*
 * Proyecto integrador Grupo 3 2017
 * 
 * Comedor Universitario
 * Allochis, José
 * Silvera, Nicolás
 * Villegas, Leandro Antú
 */
package logica;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import persistencia.Conexion;

public class Compra {

    public static int getCantidadCompras(int mes, int year) {
        Conexion conexion = Conexion.getInstance();
        ResultSet r = conexion.select("COUNT(*) as cantidad", "compras", "estado = 'procesado' AND fecha >= '" + year + "-" + mes + "-20' AND fecha <= '" + year + "-" + mes + "-28'");
        int cantidadCompras = 0;
        try {
            if (r.next()) {
                cantidadCompras = r.getInt("cantidad");
            }
            r.close();
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cantidadCompras;
    }

}
