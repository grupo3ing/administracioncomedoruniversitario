/*
 * Proyecto integrador Grupo 3 2017
 * Comedor Universitario
 *
 * Allochis, José
 * Silvera, Nicolás
 * Villegas, Leandro Antú
 */
package logica;

import java.util.ArrayList;

public class Estadistica {

    private ArrayList<Integer> datos;
    private String titulo;

    public Estadistica(ArrayList<Integer> datos, String titulo) {
        this.datos = datos;
        this.titulo = titulo;
    }

    public int getDato(int i) {
        return this.datos.get(i);
    }

    public ArrayList<Integer> getDatos() {
        return this.datos;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public static Estadistica getVentas(int mesPeriodo, int year) {
        ArrayList<Integer> res = new ArrayList<>();
        res.add(Compra.getCantidadCompras(mesPeriodo, year));
        res.add(Cliente.getClientesActivos());
        Estadistica e = new Estadistica(res, "Periodo " + mesPeriodo + "/" + year);
        return e;
    }
}
