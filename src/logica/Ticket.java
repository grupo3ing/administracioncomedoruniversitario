/*
 * Proyecto integrador Grupo 3 2017
 * 
 * Comedor Universitario
 * Allochis, José
 * Silvera, Nicolás
 * Villegas, Leandro Antú
 */
package logica;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.Barcode;
import com.itextpdf.text.pdf.BarcodeEAN;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import persistencia.Conexion;

public class Ticket implements Serializable {

    private Cliente cliente;
    private Date fecha;

    public Ticket() {
    }

    public Ticket(Cliente cliente) {
        this.cliente = cliente;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cliente != null ? cliente.hashCode() : 0);
        hash += (fecha != null ? fecha.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Ticket)) {
            return false;
        }
        Ticket other = (Ticket) object;
        if ((this.cliente == null && other.cliente != null) || (this.cliente != null && !this.cliente.equals(other.cliente))) {
            return false;
        }
        return !((this.fecha == null && other.fecha != null) || (this.fecha != null && !this.fecha.equals(other.fecha)));
    }

    @Override
    public String toString() {
        return "Ticket[ dni=" + cliente.getDni() + ", fecha=" + fecha + " ]";
    }

    public Boolean genero() {
        Conexion conexion = Conexion.getInstance();
        int mesActual = Calendar.getInstance().get(Calendar.MONTH) + 1;
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int diaActual = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        String hoy = String.valueOf(year) + "-" + String.valueOf(mesActual) + "-" + String.valueOf(diaActual);
        return conexion.exists("*", "tickets", "dni = " + cliente.getDni() + " AND date(fecha) = '" + hoy + "'");
    }

    public boolean guardar() {
        Conexion conexion = Conexion.getInstance();
        return conexion.insert("tickets", "dni", "'" + cliente.getDni() + "'", "");
    }

    public boolean imprimir() {
        Document document = new Document(PageSize.A9, 0, 0, 0, 0);
        PdfWriter writer;
        try {
            writer = PdfWriter.getInstance(document, new FileOutputStream("ticket.pdf"));
            document.open();
        } catch (FileNotFoundException | DocumentException ex) {
            Logger.getLogger(Ticket.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        int mes = Calendar.getInstance().get(Calendar.MONTH) + 1;
        int dia = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        Paragraph paragraph1 = new Paragraph(String.valueOf(dia) + "/" + String.valueOf(mes), FontFactory.getFont(FontFactory.HELVETICA, 24, Font.BOLD, new BaseColor(0, 0, 0)));
        paragraph1.setAlignment(2);
        paragraph1.setIndentationRight(5);
        Paragraph paragraph2 = new Paragraph(cliente.getNombre() + " " + cliente.getApellido(), FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, new BaseColor(0, 0, 0)));
        paragraph2.setAlignment(1);

        BarcodeEAN barcode = new BarcodeEAN();
        barcode.setCodeType(Barcode.EAN8);
        barcode.setCode(cliente.getDni());
        Image barcodeimg = barcode.createImageWithBarcode(writer.getDirectContent(), BaseColor.BLACK, BaseColor.GRAY);

        try {
            barcodeimg.scaleToFit(PageSize.A9.getWidth() - PageSize.A9.getWidth() / 4, PageSize.A9.getHeight());
            barcodeimg.setAbsolutePosition((PageSize.A9.getWidth() - barcodeimg.getScaledWidth()) / 2, 0f);

            Image logoimg = Image.getInstance(getClass().getResource("/assets/Logouniversidadgris.jpg"));
            logoimg.scaleToFit(PageSize.A9.getWidth() / 2, PageSize.A9.getHeight() / 4);
            logoimg.setAbsolutePosition(3f, PageSize.A9.getHeight() - logoimg.getScaledHeight());
            document.add(paragraph1);
            document.add(new Paragraph(" "));
            document.add(paragraph2);
            document.add(barcodeimg);
            document.add(logoimg);
            Desktop.getDesktop().open(new File(System.getProperty("user.dir") + "/ticket.pdf"));
        } catch (DocumentException | IOException ex) {
            Logger.getLogger(Ticket.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        document.close();
        return true;
    }
}
