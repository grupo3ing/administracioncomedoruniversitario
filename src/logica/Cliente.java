/*
 * Proyecto integrador Grupo 3 2017
 * 
 * Comedor Universitario
 * Allochis, José
 * Silvera, Nicolás
 * Villegas, Leandro Antú
 */
package logica;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import persistencia.Conexion;

public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;

    private String dni;
    private Integer legajo;
    private String nombre;
    private String apellido;
    private String tipo;
    private String email;
    private String direccion;
    private String telefono;
    private String estado;
    private boolean documentacion;
    private ArrayList<Beca> becas = new ArrayList();
    private String password;
    private ArrayList<Ticket> ticket;

    public Cliente(String dni) {
        this.dni = dni;
        buscarDatos();
    }

    public Cliente(String dni, int legajo, String nombre, String apellido, String tipo, String email, String direccion, String telefono, String estado, boolean documentacion, String password) {
        this.dni = dni;
        this.legajo = legajo;
        this.nombre = nombre;
        this.apellido = apellido;
        this.tipo = tipo;
        this.email = email;
        this.direccion = direccion;
        this.telefono = telefono;
        this.estado = estado;
        this.documentacion = documentacion;
        this.password = password;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Integer getLegajo() {
        return legajo;
    }

    public void setLegajo(Integer legajo) {
        this.legajo = legajo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public boolean getDocumentacion() {
        return documentacion;
    }

    public void setDocumentacion(boolean documentacion) {
        this.documentacion = documentacion;
    }

    public String becado() {
        for (Beca beca : this.becas) {
            if (beca.getPeriodo() == Calendar.getInstance().get(Calendar.YEAR)) {
                return beca.getTipo();
            }
        }
        return "no";
    }

    public Beca getBeca(int year) {
        for (Beca beca : this.becas) {
            if (beca.getPeriodo() == year) {
                return beca;
            }
        }
        return null;
    }

    public void addBeca(Beca beca) {
        becas.add(beca);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<Ticket> getTickets() {
        return ticket;
    }

    public void setTickets(ArrayList<Ticket> ticket) {
        this.ticket = ticket;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dni != null ? dni.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        return !((this.dni == null && other.dni != null) || (this.dni != null && !this.dni.equals(other.dni)));
    }

    @Override
    public String toString() {
        return "Cliente["
                + " dni=" + dni
                + ", legajo=" + legajo
                + ", nombre=" + nombre
                + ", apellido=" + apellido
                + ", tipo=" + tipo
                + ", email=" + email
                + ", direccion=" + direccion
                + ", telefono=" + telefono
                + ", estado=" + estado
                + ", documentacion=" + documentacion
                + ", becado=" + this.becado()
                + ", password=" + password
                + " ]";
    }

    private void buscarDatos() {
        Conexion conexion = Conexion.getInstance();
        ResultSet r = conexion.select("*", "clientes", "dni = '" + dni + "'");
        try {
            if (r.next()) {
                legajo = r.getInt("legajo");
                nombre = r.getString("nombre");
                apellido = r.getString("apellido");
                tipo = r.getString("tipo");
                email = r.getString("email");
                direccion = r.getString("direccion");
                telefono = r.getString("telefono");
                estado = r.getString("estado");
                documentacion = r.getShort("documentacion") == 1;
                password = r.getString("password");
            }
            r.close();
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        r = conexion.select("*", "becas", "dni = '" + dni + "'");
        try {
            while (r.next()) {
                int periodo = r.getInt("periodo");
                String tipodb = r.getString("tipo");
                becas.add(new Beca(dni, tipodb, periodo));
            }
            r.close();
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String activo() {
        String error = "";
        switch (this.becado()) {
            case "media":
                switch (estado) {
                    case "suspendido":
                        error = "Usted está suspendido del servicio del comedor";
                        break;
                    case "inactivo":
                        error = "Posee media beca pero no pagó el abono mensual";
                        break;
                }
                break;
            case "no":
                switch (estado) {
                    case "suspendido":
                        error = "Usted está suspendido del servicio del comedor";
                        break;
                    case "inactivo":
                        error = "Usted no pagó el abono mensual";
                        break;
                }
                break;
        }
        if (!documentacion) {
            error = "No presentó la documentación correspondiente";
            // si no presentó docuementación es imposible que haya adquirido el abono mensual.
        }
        return error;
    }

    public String generarTicket() {
        String error = "";
        Ticket t = new Ticket(this);
        if (!t.genero()) {
            if (t.guardar()) {
                if (!t.imprimir()) {
                    error = "Error al imprimir el ticket";
                }
            } else {
                error = "Error al registrar el ticket";
            }
        } else {
            error = "Ya generó el ticket diario";
        }
        return error;
    }

    public static Object[] getListaClientes() {
        ArrayList<String> res = new ArrayList<>();
        Conexion conexion = Conexion.getInstance();
        ResultSet r = conexion.select("dni", "clientes", "");
        try {
            while (r.next()) {
                res.add(r.getString("dni"));
            }
            r.close();
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res.toArray();
    }

    public static ArrayList<Cliente> getClientes() {
        ArrayList<Cliente> clientes = new ArrayList();
        Conexion conexion = Conexion.getInstance();
        ResultSet r = conexion.select("*", "clientes", "");
        try {
            while (r.next()) {
                String dni = r.getString("dni");
                int legajo = r.getInt("legajo");
                String nombre = r.getString("nombre");
                String apellido = r.getString("apellido");
                String tipo = r.getString("tipo");
                String email = r.getString("email");
                String direccion = r.getString("direccion");
                String telefono = r.getString("telefono");
                String estado = r.getString("estado");
                boolean documentacion = r.getShort("documentacion") == 1;
                String password = r.getString("password");
                clientes.add(new Cliente(dni, legajo, nombre, apellido, tipo, email, direccion, telefono, estado, documentacion, password));
            }
            r.close();
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clientes;
    }

    public static int getClientesActivos() {
        Conexion conexion = Conexion.getInstance();
        ResultSet r = conexion.select("COUNT(*) AS cantidad", "clientes", "documentacion = 1");
        int cantidadClientes = 0;
        try {
            if (r.next()) {
                cantidadClientes = r.getInt("cantidad");
            }
            r.close();
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cantidadClientes;
    }

    public Object[] getRow(int year) {
        return new Object[]{this.dni, this.nombre + " " + this.apellido, getBeca(year).getTipo(), getBeca(year).getPeriodo()};
    }

    public boolean guardarBeca(String tipo, int year) {
        Conexion conexion = Conexion.getInstance();
        this.addBeca(new Beca(this.dni, tipo, year));
        return conexion.insert("becas", "dni, periodo, tipo", "'" + this.dni + "', '" + year + "', '" + tipo + "'", "");
    }

    public boolean removeBeca(Beca b) {
        return this.becas.remove(b);
    }

}
