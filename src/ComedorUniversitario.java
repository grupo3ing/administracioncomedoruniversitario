
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import presentacion.LoginUI;
import presentacion.TerminalUI;

/*
 * Proyecto integrador Grupo 3 2017
 * 
 * Comedor Universitario
 * Allochis, José
 * Silvera, Nicolás
 * Villegas, Leandro Antú
 */

public class ComedorUniversitario extends javax.swing.JFrame {

    LoginUI loginui;
    TerminalUI terminalui;

    /**
     * Creates new form ComedorUniversitario
     */
    public ComedorUniversitario() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bAdmin = new javax.swing.JButton();
        bTerminal = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Comedor Universitario");
        setResizable(false);

        bAdmin.setFont(new java.awt.Font("Roboto Light", 0, 24)); // NOI18N
        bAdmin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icoAdmin.png"))); // NOI18N
        bAdmin.setText("Administración");
        bAdmin.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        bAdmin.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        bAdmin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bAdminMouseClicked(evt);
            }
        });

        bTerminal.setFont(new java.awt.Font("Roboto Light", 0, 24)); // NOI18N
        bTerminal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icoPC.png"))); // NOI18N
        bTerminal.setText("Terminal");
        bTerminal.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        bTerminal.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        bTerminal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bTerminalMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bAdmin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bTerminal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bAdmin, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(bTerminal, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void bAdminMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bAdminMouseClicked
        setVisible(false);
        loginui = new LoginUI();
        loginui.setVisible(true);
        loginui.setResizable(false);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        loginui.setLocation(dim.width / 2 - loginui.getSize().width / 2, dim.height / 2 - loginui.getSize().height / 2);
        WindowListener exitListener = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                loginui.setVisible(false);
                loginui.dispose();
                setVisible(true);
            }
        };
        loginui.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        loginui.addWindowListener(exitListener);
    }//GEN-LAST:event_bAdminMouseClicked

    private void bTerminalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bTerminalMouseClicked
        setVisible(false);
        terminalui = new TerminalUI();
        terminalui.setVisible(true);
        terminalui.setResizable(false);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        terminalui.setLocation(dim.width / 2 - terminalui.getSize().width / 2, dim.height / 2 - terminalui.getSize().height / 2);
        WindowListener exitListener = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                terminalui.setVisible(false);
                terminalui.dispose();
                setVisible(true);
            }
        };
        terminalui.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        terminalui.addWindowListener(exitListener);
    }//GEN-LAST:event_bTerminalMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ComedorUniversitario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ComedorUniversitario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ComedorUniversitario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ComedorUniversitario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ComedorUniversitario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bAdmin;
    private javax.swing.JButton bTerminal;
    // End of variables declaration//GEN-END:variables
}
