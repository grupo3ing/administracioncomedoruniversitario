/*
 * Proyecto integrador Grupo 3 2017
 * Comedor Universitario
 *
 * Allochis, José
 * Silvera, Nicolás
 * Villegas, Leandro Antú
 */
package logica;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.logging.Level;
import java.util.logging.Logger;
import persistencia.Conexion;

public class Login {
    public boolean validar(String dni, String password){
        Conexion conexion = Conexion.getInstance();
        ResultSet r = conexion.select("*", "clientes", "dni = '" + dni + "' AND tipo = 'admin'");
        Object passbd = null;
        try {
            if(r.next()){
                passbd = r.getObject("password");
            }
            r.close();
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        return password.equals(passbd);
    }
    
    public boolean isNumeric(String str) {
        NumberFormat formatter = NumberFormat.getInstance();
        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();
    }
}
