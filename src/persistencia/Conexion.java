/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexion {

    private final boolean local = true;

    private String bd;
    private String url;
    private String user;
    private String pass;
    private Statement s;
    private Connection con = null;

    private static Conexion instance;

    public Conexion() {
        this.pass = "universitario";
        this.user = "comedoruni";
        this.bd = "comedoruni";
        if (local) {
            this.url = "jdbc:mysql://localhost/" + bd;
        } else {
            this.url = "jdbc:mysql://mysql8.db4free.net:3307/" + bd;
        }
    }

    public static Conexion getInstance() {
        if (instance == null) {
            instance = new Conexion();
        }
        return instance;
    }

    private void crearConexion() throws InstantiationException, IllegalAccessException {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con = DriverManager.getConnection(url, user, pass);
            s = con.createStatement();
            s.setQueryTimeout(0);
        } catch (SQLException ex) {
            System.out.println("Hubo un problema al intentar conectarse con la base de datos " + url);
            System.out.println(ex);
        } catch (ClassNotFoundException ex) {
            System.out.println(ex);
        }
    }

    public void cerrarConexion() {
        try {
            con.close();
        } catch (SQLException e) {
            System.out.println("Error al CERRAR Base de Datos");
        }
    }

    public ResultSet select(String data, String table, String where) {
        try {
            crearConexion();
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        ResultSet rs = null;
        s = null;
        String query;
        if (where.length() > 1) {
            query = "SELECT " + data + " FROM `comedoruni`.`" + table + "` WHERE " + where + ";";
        } else {
            query = "SELECT " + data + " FROM `comedoruni`.`" + table + "`;";
        }
        try {
            s = con.createStatement();
            s.setQueryTimeout(0);
            rs = s.executeQuery(query);
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public boolean insert(String table, String fields, String values, String extra) {
        try {
            crearConexion();
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        s = null;
        boolean error = false;
        String query;
        if (extra.length() > 1) {
            query = "INSERT INTO " + table + " (" + fields + ") VALUES (" + values + ") " + extra + ";";
        } else {
            query = "INSERT INTO " + table + " (" + fields + ") VALUES (" + values + ");";
        }
        //System.out.println(query);
        try {
            s = con.createStatement();
            s.setQueryTimeout(0);
            int i = s.executeUpdate(query);
            error = i == 0;
        } catch (SQLException e) {
        }
        return !error;
    }

    public boolean exists(String table, String fields, String values) {
        ResultSet r = select(table, fields, values);
        try {
            if (!r.next()) {
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public void delete(String table, String where) {
        try {
            crearConexion();
        } catch (InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        s = null;
        String query = "DELETE FROM " + table + " WHERE " + where + ";";
        try {
            s = con.createStatement();
            s.setQueryTimeout(0);
            s.executeUpdate(query);
        } catch (SQLException e) {
        }
    }
}
