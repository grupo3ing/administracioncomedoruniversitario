/*
 * Proyecto integrador Grupo 3 2017
 * 
 * Comedor Universitario
 * Allochis, José
 * Silvera, Nicolás
 * Villegas, Leandro Antú
 */
package logica;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.border.LineBorder;

public class MenuMes {

    private Calendar periodo;
    private ArrayList<BotonMenu> comidas;

    public MenuMes() {
        comidas = new ArrayList(42);
        for (int i = 0; i < 42; i++) {
            comidas.add(new BotonMenu());
        }
        periodo = Calendar.getInstance();
        periodo.set(periodo.get(Calendar.YEAR), periodo.get(Calendar.MONTH), 1);
        this.buildCalendario();
    }

    public Calendar getPeriodo() {
        return this.periodo;
    }

    public ArrayList<BotonMenu> getComidas() {
        return this.comidas;
    }

    public void cambiarPeriodo(String cambio) {
        comidas = new ArrayList(42);
        for (int i = 0; i < 42; i++) {
            comidas.add(new BotonMenu());
        }
        if ("atras".equals(cambio)) {
            periodo.add(Calendar.MONTH, -1);
        } else {
            periodo.add(Calendar.MONTH, 1);
        }
        this.buildCalendario();
    }

    public void buildCalendario() {

        Calendar cal = (Calendar) periodo.clone();
        int fday = cal.get(Calendar.DAY_OF_WEEK);
        int daysm = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.add(Calendar.MONTH, -1);
        int daysprevm = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        cal.add(Calendar.MONTH, 1);
        int daysadded = 0;
        int j = 0;

        ArrayList<MenuDiario> menus = MenuDiario.getMenusMes(cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR));

        ArrayList<BotonMenu> activos = new ArrayList();

        if (fday == 1) {
            for (int i = daysprevm - 6; i <= daysprevm; i++) {
                comidas.get(j).getButton().setBackground(new Color(255, 255, 255));
                comidas.get(j).getButton().setFont(new Font("sansserif", 1, 18));
                comidas.get(j).getButton().setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
                comidas.get(j).getButton().setBorderPainted(false);
                comidas.get(j).getButton().setText(String.valueOf(i));
                comidas.get(j).getButton().setEnabled(false);
                j++;
            }
        }
        for (int i = daysprevm - fday + 2; i <= daysprevm; i++) {
            comidas.get(j).getButton().setBackground(new Color(255, 255, 255));
            comidas.get(j).getButton().setFont(new Font("sansserif", 1, 18));
            comidas.get(j).getButton().setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
            comidas.get(j).getButton().setBorderPainted(false);
            comidas.get(j).getButton().setText(String.valueOf(i));
            comidas.get(j).getButton().setEnabled(false);
            j++;
            daysadded++;
        }
        int i;
        for (i = 1; i <= 7 - daysadded; i++) {
            comidas.get(j).getButton().setBackground(new Color(255, 255, 255));
            comidas.get(j).getButton().setFont(new Font("sansserif", 1, 18));
            comidas.get(j).getButton().setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
            comidas.get(j).getButton().setBorderPainted(false);

            comidas.get(j).getButton().setText(String.valueOf(i));

            if (j % 7 == 0 || (j + 1) % 7 == 0) {
                comidas.get(j).getButton().setEnabled(false);
            } else {
                comidas.get(j).setFecha(i, cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR));
                comidas.get(j).getButton().setEnabled(true);
                comidas.get(j).getButton().setFocusable(false);
                activos.add(comidas.get(j));
            }
            j++;
        }

        for (int k = i; k <= daysm; k++) {
            comidas.get(j).getButton().setBackground(new Color(255, 255, 255));
            comidas.get(j).getButton().setFont(new Font("sansserif", 1, 18));
            comidas.get(j).getButton().setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
            comidas.get(j).getButton().setBorderPainted(false);
            comidas.get(j).getButton().setText(String.valueOf(k));

            if (j % 7 == 0 || (j + 1) % 7 == 0) {
                comidas.get(j).getButton().setEnabled(false);
            } else {
                comidas.get(j).setFecha(k, cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR));
                comidas.get(j).getButton().setEnabled(true);
                comidas.get(j).getButton().setFocusable(false);
                activos.add(comidas.get(j));
            }
            j++;
        }
        i = 1;
        for (int k = j; k < 42; k++) {
            comidas.get(k).getButton().setBackground(new Color(255, 255, 255));
            comidas.get(k).getButton().setFont(new Font("sansserif", 1, 18));
            comidas.get(k).getButton().setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
            comidas.get(k).getButton().setBorderPainted(false);
            comidas.get(k).getButton().setText(String.valueOf(i));
            comidas.get(k).getButton().setEnabled(false);
            i++;
        }

        for (BotonMenu b : activos) {
            for (MenuDiario m : menus) {
                if (b.getDay() == m.getDay()) {
                    b.setMenu(m);
                    b.getButton().setToolTipText(m.getTooltipText());
                    b.getButton().setBackground(new Color(255, 152, 0));
                    break;
                }
            }
            b.setAction();
        }

    }
}
