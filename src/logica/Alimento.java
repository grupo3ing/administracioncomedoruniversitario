/*
 * Proyecto integrador Grupo 3 2017
 * 
 * Comedor Universitario
 * Allochis, José
 * Silvera, Nicolás
 * Villegas, Leandro Antú
 */
package logica;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import persistencia.Conexion;

public class Alimento implements Comparable<Alimento> {

    private String tipo;
    private String nombre;

    public Alimento(String nombre, String tipo) {
        this.tipo = tipo;
        this.nombre = nombre;
    }

    public String getTipo() {
        return this.tipo;
    }

    public String getNombre() {
        return this.nombre;
    }

    public static ArrayList<Object[]> getAlimentos() {
        ArrayList<Object[]> res = new ArrayList();
        ArrayList<Alimento> alimentos = new ArrayList();
        Conexion conexion = Conexion.getInstance();
        ResultSet r = conexion.select("*", "alimentos", "");
        try {
            while (r.next()) {
                alimentos.add(new Alimento(r.getString("nombre"), r.getString("tipo")));
            }
            r.close();
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }

        ArrayList<String> tipospp = new ArrayList();

        for (Alimento a : alimentos) {
            if (!"Postre".equals(a.getTipo()) && !"Guarnicion".equals(a.getTipo()) && !"Salsa".equals(a.getTipo())) {
                if (!tipospp.contains(a.getTipo())) {
                    tipospp.add(a.getTipo());
                }
            }
        }

        Collections.sort(tipospp, String.CASE_INSENSITIVE_ORDER);

        ArrayList<Alimento> comidas = new ArrayList();

        ArrayList<String> platosprincipales = new ArrayList<>();
        platosprincipales.add("-- Platos principales --");
        ArrayList<String> guarniciones = new ArrayList<>();
        guarniciones.add("-- Guarniciones --");
        ArrayList<String> salsas = new ArrayList<>();
        salsas.add("-- Salsas --");
        ArrayList<String> postres = new ArrayList<>();
        postres.add("-- Postres --");

        for (Alimento a : alimentos) {
            switch (a.getTipo()) {
                case "Guarnicion":
                    guarniciones.add(a.getNombre());
                    break;
                case "Salsa":
                    salsas.add(a.getNombre());
                    break;
                case "Postre":
                    postres.add(a.getNombre());
                    break;
                default:
                    comidas.add(a);
            }
        }

        Collections.sort(comidas);

        for (String t : tipospp) {
            platosprincipales.add("-- " + t + " --");
            for (Alimento a : comidas) {
                if (t.equals(a.getTipo())) {
                    platosprincipales.add(a.getNombre());
                }
            }
        }

        Collections.sort(guarniciones, String.CASE_INSENSITIVE_ORDER);
        Collections.sort(salsas, String.CASE_INSENSITIVE_ORDER);
        Collections.sort(postres, String.CASE_INSENSITIVE_ORDER);

        res.add(platosprincipales.toArray());
        res.add(salsas.toArray());
        res.add(guarniciones.toArray());
        res.add(postres.toArray());

        return res;

    }

    @Override
    public int compareTo(Alimento o) {
        return this.nombre.compareTo(o.getNombre());
    }
}
